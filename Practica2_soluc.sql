﻿USE practica2;

/** 
  1. Indicar el número de ciudades que hay en la tabla ciudades **/

SELECT COUNT(*) nciudades 
FROM ciudad c;

/** 2. Indicar el nombre de las ciudades 
   que tengan una población por encima de la población media**/
  
  SELECT c.nombre 
    FROM ciudad c 
    WHERE c.población>(
      SELECT AVG(c1.población) 
      FROM 
        ciudad c1);
/* 3. Indicar el nombre de las ciudades 
      que tengan una población por debajo de la población media */

SELECT c.nombre 
    FROM ciudad c 
    WHERE c.población<(
      SELECT AVG(c1.población) 
      FROM 
        ciudad c1);
/* 4. Indicar el nombre de la ciudad con la población máxima */
  
  SELECT c.nombre
    FROM ciudad c
    WHERE c.población=(
      SELECT MAX(c1.población) maximo  
      FROM ciudad c1);
/* 5. Indicar el nombre de la ciudad con la población mínima */
  
  SELECT c.nombre
    FROM ciudad c
    WHERE c.población=(
      SELECT MIN(c1.población) maximo  
      FROM ciudad c1);
  /* 6. Indicar el nº de ciudades 
        que tengan una población por encima de la población media.*/
  
  SELECT COUNT(*) nciudades 
    FROM ciudad c 
    WHERE c.población>(
      SELECT AVG(c1.población) 
      FROM 
        ciudad c1);
  
  /* 7. Indicar el nº de personas que viven en cada ciudad.*/
    
  SELECT p.ciudad, COUNT(*) nciudades 
    FROM persona p
    GROUP BY p.ciudad;

/* 8. Utilizando la tabla trabaja indicarme cuantas personas 
      trabajan en cada una de las compañias */
  
  SELECT t.compañia, COUNT(*) ntrabajadores 
    FROM trabaja t
    GROUP BY t.compañia;

/* 9. Indicarme la compañía que más trabajadores tiene */

-- C1 listar el nº de trabajadores por cada compañía

SELECT t.compañia, COUNT(*) ntrabajadores 
  FROM trabaja t
  GROUP BY t.compañia;

-- C2 listar el nº de trabajadores máximo por cada compañía
  
  SELECT MAX(c1.ntrabajadores) maximo
    FROM (
      SELECT t.compañia, COUNT(*) ntrabajadores 
        FROM trabaja t
        GROUP BY t.compañia
    ) c1;
    
-- Consulta Final
 
 SELECT t.compañia, COUNT(*) ntrabajadores 
  FROM trabaja t
  GROUP BY t.compañia
  HAVING ntrabajadores=(
    SELECT MAX(c1.ntrabajadores) maximo
      FROM (
        SELECT t.compañia, COUNT(*) ntrabajadores 
          FROM trabaja t
          GROUP BY t.compañia
       )c1
      );
 
 /* 10. Indicarme el salario medio de cada una de las compañias*/

SELECT t.compañia, AVG(t.salario) salariomedio 
  FROM trabaja t
  GROUP BY t.compañia;
 
 /* 11. Listar el nombre de los trabajadores y la población donde viven */

SELECT p.nombre, c.población
  FROM persona p 
  JOIN ciudad c 
  ON p.ciudad= c.nombre;      

/* 12. Listar el nombre de los trabajadores, 
       la calle donde vive y la población de la ciudad donde vive */

SELECT p.nombre, p.calle, c.población 
   FROM persona p 
   JOIN ciudad c 
   ON p.ciudad= c.nombre;   

/* 13. Listar el nombre de los trabajadores, la ciudad donde vive 
 y la ciudad donde está la compañía para la que trabaja */

SELECT p.nombre,p.ciudad AS empleadosviven, c.ciudad AS empleadostrabajan
  FROM persona p 
  JOIN trabaja t 
  ON p.nombre= t.persona 
  JOIN compañia c 
  ON t.compañia= c.nombre;

/* 14. Realizar el algebra relacional y explicar la siguiente consulta */

SELECT p.nombre 
  FROM persona p, trabaja t, compañia c 
  WHERE p.nombre= t.persona 
  AND t.compañia = c.nombre 
  AND c.ciudad = p.ciudad 
  ORDER BY p.nombre;

/* 15. Listar el nombre de la persona y el nombre de su supervisor */

SELECT s.persona, s.supervisor 
  FROM supervisa s;

/* 16. Listar el nombre de la persona, 
  el nombre de su supervisor 
  y las ciudades donde residen cada una de ellos*/

SELECT p.nombre, s.supervisor, p1.ciudad  
  FROM persona p 
  JOIN supervisa s ON p.nombre= s.persona 
  JOIN persona p1 ON s.supervisor= p1.nombre;

/* 17. Indicarme el número de ciudades distintas 
  que hay en la tabla compañía */

SELECT c.ciudad, COUNT(*)nciudades 
  FROM compañia c
  GROUP BY c.ciudad;

-- Lista de las ciudades de la tabla compañia

SELECT DISTINCT c.ciudad
  FROM compañia c;
  
-- C. Final

SELECT COUNT(*) nciudades 
  FROM (
    SELECT DISTINCT c.ciudad
      FROM compañia c) c1;

/* 18. Indicarme el número de ciudades distintas 
  que hay en la tabla personas */

-- C1. Nº de ciudades distintas

SELECT DISTINCT p.ciudad 
    FROM persona p;

-- C. Final  

SELECT COUNT(*) nciudades 
  FROM (
    SELECT DISTINCT p.ciudad
      FROM persona p) c1;

/* 19. Indicarme el nombre de las personas 
  que trabajan para FAGOR */
-- C1. Nº de personas que trabajan

SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia='FAGOR';

/* 20. Indicarme el nombre de las personas 
  que no trabajan para FAGOR */



-- C1. Nº de personas que trabajan en FAGOR

SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia='FAGOR';

-- C. Final

SELECT p.nombre 
    FROM persona p 
      WHERE p.nombre NOT IN (
        SELECT t.persona 
          FROM trabaja t 
          WHERE t.compañia='FAGOR'
      );
/* 21. Indicarme el número de personas que trabajan para INDRA  */

  SELECT COUNT(*) nº_trabajadores
    FROM trabaja t 
    WHERE t.compañia='INDRA';

/* 22. Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA*/

  -- C1. Trabajadores de Fagor
  
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia='FAGOR';

  -- C2. Trabajadores de Indra
  
  SELECT t.persona
    FROM trabaja t 
    WHERE t.compañia='INDRA';

-- Consulta Final
  
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia='FAGOR'
  UNION
  SELECT t.persona
    FROM trabaja t 
    WHERE t.compañia='INDRA';

/* 23. Listar la población donde vive cada persona, sus salario, 
       su nombre y la compañía para la que trabaja. 
       Ordenar la salida por nombre de la persona 
       y por salario de forma descendente. */

SELECT c.población, t.salario, p.nombre, t.compañia 
  FROM ciudad c 
  JOIN persona p ON c.nombre = p.ciudad 
  JOIN trabaja t ON p.nombre=t.persona
  ORDER BY p.nombre DESC, t.salario DESC;
   

  


















    
